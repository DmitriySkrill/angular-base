import {Component, Input, OnInit} from '@angular/core';
import {Card} from '../app.component';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
})

export class CardComponent implements OnInit {

  @Input() card: Card;
  @Input() ind;

  date: Date = new Date();
  title = 'my card title';
  text = 'my card text';
  number = 42;
  array = [1, 1, 2, 3, 5, 8, 13];
  obj = {
    name: 'sdf', info: {age: 25, job: 'sdg'}
  };
  imgUrl = 'https://angular.io/assets/images/logos/angular/angular.png';
  disabled = false;
  textColor = 'black';

  ngOnInit(): void {
    setTimeout(() => {
      this.imgUrl = 'https://miro.medium.com/max/400/1*zyNSb0UXhP8TfxYbj-GNWg.png';
      this.disabled = true;
    }, 3000);
  }

  onInput(event: any): void {
    this.card.title = event.target.value;
  }

  onMyInput(value): void {
    this.card.title = value;
  }

  changeTitle(): void {
    this.card.title = 'title changed';
  }

  changeHandler(): void {
    console.log(this.card.title);
  }

  getInfo(): string {
    return 'this is my info';
  }
}
